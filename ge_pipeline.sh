#!/bin/sh
##### Constants

TITLE="Automated Pipeline by Andras Balogh"
RIGHT_NOW=$(date +"%x %r %Z")

##### Functions

dl_sratoolkit()
{
	echo "Downloading SRA Toolkit..."
    cd $SCRATCH
    mkdir -p pfetch_pathway
    cd $HOME
    wget https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.8.2-1/sratoolkit.2.8.2-1-centos_linux64.tar.gz
    tar -zxvf sratoolkit.2.8.2-1-centos_linux64.tar.gz
    rm -r sratoolkit.2.8.2-1-centos_linux64.tar.gz
    mv sratoolkit.2.8.2-1-centos_linux64 sratoolkit
    rm -r sratoolkit.2.8.2-1-centos_linux64
	cp $HOME/ge_pipeline/pfetch_dl_path.exp $HOME/sratoolkit/bin/
    cd $HOME/sratoolkit/bin/
	pfetch_dl_path.exp
	echo "Done downloading SRA Toolkit."
}

# Our coral reference genome
dl_spist_genome()
{
    echo "Downloading reference genome..."
    cd $HOME
    mkdir -p stylophora_pistillata_genome
    cd $HOME/stylophora_pistillata_genome
    wget "http://spis.reefgenomics.org/download/Spis.genome.scaffold.final.fa.gz"
    gunzip Spis.genome.scaffold.final.fa.gz
	echo "Building..."
	bowtie2-build Spis.genome.scaffold.final.fa Spis.genome.scaffold.final.fa
    echo "Done downloading reference genome."
}

# Creating accession run list of SRA projects
create_runlist()
{
	echo "Creating runList.txt..."
    cd $HOME/ge_pipeline/
	#wget "$link"
    cut -f9 SraRunTable.txt > runList.txt
    sed '1d' runList.txt > tmpfile; mv tmpfile runList.txt
    cd $SCRATCH
	mkdir -p fastqs
	mv $HOME/ge_pipeline/runList.txt $SCRATCH/fastqs/runList.txt
	echo "Done creating runList.txt"
}

prefetch_dl()
{
	echo "Creating and running prefetch download script..."
    cd $SCRATCH/fastqs
    >download
    while read srr
    do echo "$PREFETCH $srr" >> download
    done < runList.txt
    sh download
	echo "Done creating and running prefetch download script."
}

dump_fastq()
{
	echo "Dumping fastq files..."
    cd $SCRATCH/fastqs
    >dump
    while read srr
    do echo "$FQDUMP --split-files ${PREFETCH_PATH}/sra/${srr}.sra" >> dump
    done < runList.txt
    sh dump
	echo "Done dumping fastq files."
}

fastq_quality()
{
	echo "Submitting fastq job..."
	cd $SCRATCH/fastqs
    module load fastqc
    mkdir -p Fastqc_Results_raw/
    > runFQC
    for file in *.fastq
    do echo "fastqc -o Fastqc_Results_raw/ -f fastq $file" >> runFQC
    done
    python $HOME/ge_pipeline/scripts/launcher_creator.py -n runFQC -j runFQC -q normal -N 1 -a $ALLO -e $EMAIL -t 02:00:00
    sbatch --wait runFQC.slurm
	echo "Done with fastq job."
}

raw_read_count()
{
	echo "Getting raw read count..."
	cd $SCRATCH/fastqs
	wc -l *.fastq | awk 'BEGIN {print "file\tlineCount\treadCount"};{print $2"\t"$1"\t"$1/4}' > raw_read_counts.tsv
	echo "Done with raw read count."
}

# Trimming involves removing adapter sequences from ends of reads
# that we stuck on for our RAD reads. Only done if RNA fragment is 
# long enough (50+), otherwise not used. Also checking for minimum 
# quality of 30. 
trimming()
{
	echo "Trimming..."
	cd $SCRATCH/fastqs
	>trimpe
	for file in *_2.fastq
	do echo "cutadapt \
	-b GATCGGAAGAGCACACGTCTGAACTCCAGTCAC \
	-B GATCGGAAGAGCACACGTCTGAACTCCAGTCAC \
	-b GTGACTGGAGTTCAGACGTGTGCTCTTCCGATC \
	-B GTGACTGGAGTTCAGACGTGTGCTCTTCCGATC \
	-b AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT \
	-B AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT \
	-b AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT \
	-B AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT \
	--minimum-length 50 \
	-q 30 \
	-o ${file/_2.fastq/}_1.trim \
	-p ${file/_2.fastq/}_2.trim \
	${file/_2.fastq/}_1.fastq \
	$file" >> trimpe
	done
	python $HOME/ge_pipeline/scripts/launcher_creator.py -n trimpe -j trimpe -a $ALLO -e $EMAIL -q normal -t 8:00:00 -N 2 -w 24
	sbatch --wait trimpe.slurm
	echo "Done trimming."
}

trim_read_count()
{
	echo "Getting trimmed read count..."
	cd $SCRATCH/fastqs
	wc -l *.trim | awk 'BEGIN {print "file\tlineCount\treadCount"};{print $2"\t"$1"\t"$1/4}' > trimmed_read_counts.tsv
	echo "Done getting trimmed read count."
}

post_trim_quality()
{
	echo "Checking post-trimming quality..."
	cd $SCRATCH/fastqs
	module load fastqc
	mkdir -p Fastqc_Results_postTrim/
	> runFQC2
	for file in *.trim
	do echo "fastqc -o Fastqc_Results_postTrim/ -f fastq $file" >> runFQC2
	done
	python $HOME/ge_pipeline/scripts/launcher_creator.py -n runFQC2 -j runFQC2 -q normal -N 1 -w 24 -a $ALLO -e $EMAIL -t 02:00:00
	sbatch --wait runFQC2.slurm
	echo "Done checking post-trim quality."
}

# Mapping involves using alignment sequence algorithms (usually to align with 
# a reference genome). Using bowtie (not Burrows-Wheeler Aligner or STAR) 
# because of local runs instead of global alignment. 
# STAR is actually better for normal RNA-seq because it can be fed a 
# general feature file (gff - a more specific reference genome file that includes 
# gene locations). We use TagSeq for shorter reads. 
# STAR better for general use IFF with a gff. 
map_paired_ends()
{
	echo "Mapping paired RNA..."
	cd $SCRATCH/fastqs
	module load bowtie
	>mappe
	for file in *_2.trim
	do echo "bowtie2 -x $REF_STYLO_GENOME -1 ${file/_2.trim/}_1.trim -2 $file --local -p 12 -S ${file/_2.trim/}.sam" >> mappe
	done
	echo "Done mapping paired RNA."
	echo "Paired end Mapping efficiences:"
	grep "overall alignment rate" mappe.e* > mapping_pe_efficiencies.txt
}

map_single_ends()
{
	echo "Mapping single ends..."
	cd $SCRATCH/fastqs
	module load bowtie
	>mapse
	for file in *.trim
	do echo "bowtie2 -x $REF_STYLO_GENOME -U $file -S ${file/_1.trim/}.sam" >> mapse
	done
	echo "Done mapping single ends."
	echo "Single end Mapping efficiences:"
	grep "overall alignment rate" mapse.e* > mapping_se_efficiencies.txt
}

# Outputs encoded qualities of alignments 
# Done for initial counts and duplicate-removed counts. 
sam_files()
{
	echo "Converting, sorting, and indexing sam files..."
	cd $SCRATCH/fastqs
	samSort.py *.sam > convertSort
	python $HOME/ge_pipeline/scripts/launcher_creator.py -n convertSort -j convertSort -q normal -N 2 -w 12 -a $ALLO -t 04:00:00 -e $EMAIL
	sbatch --wait convertSort.slur
	echo "Done indexing sam files."
}

initial_alignment_counts()
{
	echo "Getting initial alignment counts..."
	cd $SCRATCH/fastqs
	for file in *.bam
	do echo "samtools flagstat $file > ${file/.bam/}_flagstats.txt" >> getInitialAlignment
	done
	echo "Done with initial alignment."
}

# Removes duplicate reads. Either keeps higher quality alignment or 
# first alignment. Basically throws away any repeats because they are most likely from PCR. 
remove_duplicates()
{
	echo "Removing duplicates..."
	cd $SCRATCH/fastqs
	module load picard
	>removeDups;for file in *sorted.bam; do echo "java -Xms4g -jar /home1/apps/intel17/picard/2.11.0/build/libs/picard.jar MarkDuplicates -h\
	 INPUT=$file\
	 OUTPUT=${file/.sorted.bam/}_dupsRemoved.bam\
	 METRICS_FILE=${file/.sorted.bam/}_dupMetrics.txt\
	 REMOVE_DUPLICATES=true" >> removeDups; done
	python $HOME/ge_pipeline/scripts/launcher_creator.py -n removeDups -j removeDups -t 12:00:00 -q normal -a $ALLO -e $EMAIL -N 4 -w 2
	sbatch --wait removeDups.slurm
	echo "Done removing duplicates."
}

removal_metrics()
{
	echo "Gathering removal metric data..."
	cd $SCRATCH/fastqs
	>dupRemovalMetrics.tsv
	for file in *dupMetrics.txt
	do pct=$(sed '8q;d' $file | cut -f 8)
	echo -e "$file\t$pct" >> dupRemovalMetrics.tsv
	done
	echo "Done getting removal metrics."
}

reindex()
{
	echo "Re-indexing duplicate-removed bam..."
	cd $SCRATCH/fastqs
	>reIndex;for file in *dupsRemoved.bam; do echo "samtools index $file" >> reIndex;done
	python $HOME/ge_pipeline/scripts/launcher_creator.py -n reIndex -j reIndex -q normal -t 00:30:00 -a $ALLO -e $EMAIL -N 1 -w 24
	sbatch --wait reIndex.slurm
	echo "Done re-indexing."
}

post_dup_align()
{
	echo "Getting post- duplicate-removed alignment counts..."
	cd $SCRATCH/fastqs
	for file in *dupsRemoved.bam
	do echo "samtools flagstat $file > ${file/.bam/}_post_dedup_flagstats.txt" >> getDupRemAlignment
	done
	echo "Done with post- dup-removed alignment."
}

# Outputs encoded qualities of alignments 
# Done for initial counts and duplicate-removed counts. 
convert_sam()
{
	echo "Converting bam to sam..."
	cd $SCRATCH/fastqs
	>convert
	for file in *Removed.bam
	do echo "samtools sort -n -O sam -o ${file/_sorted.bam_dupsRemoved.bam/}.sam $file" >> convert
	done
	python $HOME/ge_pipeline/scripts/launcher_creator.py -n convert -j convert -q normal -N 1 -w 24 -a $ALLO -e $EMAIL -t 13:30:00
	sbatch --wait convert.slurm
	echo "Done converting bam to sam."
}

# Many counts for reads to genes 
run_counts()
{
	echo "Running counts..."
	cd $SCRATCH/fastqs
	>docounts; for file in *.sam; do echo "htseq-count -t gene -i ID -m intersection-nonempty $file Spis.genome.annotation.gff3 > ${file/sam/counts.txt}">>docounts;done
	python $HOME/ge_pipeline/scripts/launcher_creator.py -n docounts -j docounts -q normal -t 10:00:00 -N 2 -w 24 -a $ALLO -e $EMAIL
	sbatch --wait docounts.slurm
	echo "Done running counts."
}

assemble_counts()
{
	echo "Creating single counts table..."
	cd $SCRATCH/fastqs
	assemble_htseq_counts.py -i *counts.txt -o pungitius_rnaseq_baseline_counts.tsv
	echo "Done creating single counts table."
}

org_results()
{
	echo "Gathering data results files to /Results..."
	cd $SCRATCH/fastqs
	mkdir -p Results
	mkdir -p $HOME/Results
	mkdir -p $WORK/Results
	cp raw_read_counts.tsv Results/
	cp trimmed_read_counts.tsv Results/
	cp mapping_efficiencies.txt Results/
	cp *flagstats.txt Results/
	cp dupRemovalMetrics.tsv Results/
	cp *post_dedup_flagstats.txt Results/
	cp pungitius_rnaseq_baseline_counts.tsv Results/
	cp -r Results/* $HOME/Results/
	cp -r Results/* $WORK/Results/
	echo "Done organizing results."
}

# bisulfite walkthrough will be very similar 
# same for all functions until bismarck is used <- does mapping for 
# you and uses bowtie 
# CT methylation counts 

#cat <<- _EOF_

echo $hello
echo $TITLE
echo $RIGHT_NOW
echo "If this is your first time running this script and you want to install necessary tools, 
enter 'ge_install' otherwise, enter 'Start':"
# followed by the link to your run table:"
read entry
# add numbered options for each run
if [ $entry = "Start" ]; then
	#echo "Enter a link to your run table:"
	#read link ## need proper query/commands for downloading run tables from terminal
	echo $(date +"%x %r %Z")
	(create_runlist)
	echo $(date +"%x %r %Z")
	(prefetch_dl)
	echo $(date +"%x %r %Z")
	(dump_fastq)
	echo $(date +"%x %r %Z")
	(fastq_quality)
	echo $(date +"%x %r %Z")
	(raw_read_count)
	echo $(date +"%x %r %Z")
	(trimming)
	echo $(date +"%x %r %Z")
	(trim_read_count)
	echo $(date +"%x %r %Z")
	(post_trim_quality)
	echo $(date +"%x %r %Z")
	(map_paired_ends)
	#echo $(date +"%x %r %Z")
	#(map_single_ends)
	echo $(date +"%x %r %Z")
	(sam_files)
	echo $(date +"%x %r %Z")
	(initial_alignment_counts)
	echo $(date +"%x %r %Z")
	(remove_duplicates)
	echo $(date +"%x %r %Z")
	(removal_metrics)
	echo $(date +"%x %r %Z")
	(reindex)
	echo $(date +"%x %r %Z")
	(post_dup_align)
	echo $(date +"%x %r %Z")
	(convert_sam)
	echo $(date +"%x %r %Z")
	(run_counts)
	echo $(date +"%x %r %Z")
	(assemble_counts)
	echo $(date +"%x %r %Z")
	(org_results)
	echo $(date +"%x %r %Z")
elif [ $entry = "ge_install" ]; then
	echo $(date +"%x %r %Z")
	(dl_sratoolkit)
	echo $(date +"%x %r %Z")
	(dl_spist_genome)
	echo $(date +"%x %r %Z")
	echo "Do you want to run the script (Y/N)?"
	read ans
	if [ $ans = "Y" ] || [ $ans = "y" ]; then
		#echo "Enter a link to your run table:"
    	#read link ## need proper query/commands for downloading run tables from terminal
	    echo $(date +"%x %r %Z")
		(create_runlist)
		echo $(date +"%x %r %Z")
		(prefetch_dl)
		echo $(date +"%x %r %Z")
		(dump_fastq)
		echo $(date +"%x %r %Z")
		(fastq_quality)
		echo $(date +"%x %r %Z")
		(raw_read_count)
		echo $(date +"%x %r %Z")
		(trimming)
		echo $(date +"%x %r %Z")
		(trim_read_count)
		echo $(date +"%x %r %Z")
		(post_trim_quality)
		echo $(date +"%x %r %Z")
		(map_paired_ends)
		#echo $(date +"%x %r %Z")
		#(map_single_ends)
		echo $(date +"%x %r %Z")
		(sam_files)
		echo $(date +"%x %r %Z")
		(initial_alignment_counts)
		echo $(date +"%x %r %Z")
		(remove_duplicates)
		echo $(date +"%x %r %Z")
		(removal_metrics)
		echo $(date +"%x %r %Z")
		(reindex)
		echo $(date +"%x %r %Z")
		(post_dup_align)
		echo $(date +"%x %r %Z")
		(convert_sam)
		echo $(date +"%x %r %Z")
		(run_counts)
		echo $(date +"%x %r %Z")
		(assemble_counts)
		echo $(date +"%x %r %Z")
		(org_results)
		echo $(date +"%x %r %Z")
	fi
else
	echo "Non-input."
	exit
fi

#_EOF_
