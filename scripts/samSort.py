#!/usr/bin/env python
'''prints samtools commands to convert, sort, and index a sam alignment file'''

import sys
fileList = sys.argv[1:]
for file in fileList:
    fileName = file
    prefix = fileName[0:-4]
    bamName = prefix + ".bam"
    sortedBamName = prefix + "_sorted.bam"
    
    print "samtools sort -O BAM -o {} {} && samtools index {}".format(sortedBamName, fileName, sortedBamName)
