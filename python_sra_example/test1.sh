#!/bin/bash
# 
# simple example script to test runtime scaling

cacheDir=/scratch/00161/karl/sra-files/

for i in 1 2 4 8 16 24; do
    rm -f ${cacheDir}/*.fastq
    ./coral_analysis_tool.py --threads $i
done
