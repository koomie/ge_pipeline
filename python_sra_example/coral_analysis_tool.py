#!/usr/bin/env python3
import logging
import configparser
import argparse
import coloredlogs
import os
import multiprocessing
import mytimer
import urllib.request
import humanize
import subprocess

#---
# Simple error wrapper to include exit
def ERROR(output):
    logging.error(output)
    exit(1)
    
#---
# Main worker class to help facilitate download of NCBI files and sequencing analysis
class coral_analysis_tool(object):

    #---
    # class init
    def __init__(self):
        coloredlogs.install(level='INFO',fmt="%(message)s")
        logging.info("\n" + '--' * 20)
        logging.info("Sequencing Analysis Tool");
        logging.info('--' * 20)

        # parse command-line arguments
        parser = argparse.ArgumentParser()
        parser.add_argument("--configFile",help="runtime config file name (default = config)",type=str,default='config')
        parser.add_argument("--threads",help="override maxThreads option from config file",type=int)
        parser.add_argument("--no-cache",dest='cache',help="override and invalidate local file cache",action="store_false")
        parser.set_defaults(cache=True)
        args = parser.parse_args()

        self.configFileName=args.configFile

        if not args.cache:
            self.cacheDisableOverride = True
        else:
            self.cacheDisableOverride = False

        if args.threads:
            self.overrideThreads = args.threads
        else:
            self.overrideThreads = 0

        # init a simple timer
        self.atimer = mytimer.timer()

        self.runConfig = None
        self.maxDownloadThreads = 1
        self.maxThreadsAllowed  = 32

        self.fileList = []

    #--
    # Check if [cached] filepath exists and filesize is > 0
    def checkFilePath(self,path):
        message = " checking if " + path + " is available locally..."

        if not os.path.isfile(path):
            logging.info("[-caching]:" + message + "no")
            return False
        else:
            fileSize=os.path.getsize(path)
            if fileSize:
                logging.info("[ caching]:" + message + "yes (%s)" % humanize.naturalsize(fileSize))
                return True
            else:
                logging.warn("zero file size detected")
                return False


    #---
    # parse runtime configuration file
    def parseConfig(self):

        logging.warn("\nReading runtime config information from file -> %s" % self.configFileName)

        if os.path.isfile(self.configFileName):
            self.runConfig = configparser.ConfigParser(inline_comment_prefixes='#',interpolation=configparser.ExtendedInterpolation())
            try:
                self.runConfig.read(self.configFileName)
            except configparser.DuplicateSectionError:
                ERROR("\nERROR: Duplicate section detected in configfile: %s" % configFile)
            except:
                ERROR("ERROR; Unable to parse runtime config file: %s" % configFile)

            # read global settings
            try:
                self.ncbiUrl         = self.runConfig.get('global','ncbi_url')
                self.cacheDir        = self.runConfig.get('global','cacheDir')
                if self.cacheDisableOverride:
                    logging.warn("\n[note: caching disabled via override with command-line arg]\n")
                    self.enableCaching = False
                else:
                    self.enableCaching   = self.runConfig.getboolean('global','cacheDownload',fallback=False)

                self.sraFileName     = self.runConfig.get('global','sraFileName')
                
                if self.overrideThreads > 0:
                    logging.warn("\n[note: maxThreads overridden with command-line option]\n")
                    self.downloadThreads = self.overrideThreads
                else:
                    self.downloadThreads = int(self.runConfig.get('global','maxDownloadThreads'))
                self.sraPath         = self.runConfig.get('global','sratoolsDir')
                self.fastqDumpOpts   = self.runConfig.get('global','fastqDumpOpts')



            except:
                ERROR("Unable to parse global runtime settings")

            logging.info("--> NCBU download url         = %s" % self.ncbiUrl)
            logging.info("--> SRA input filename        = %s" % self.sraFileName)
            logging.info("--> Enable SRA caching        = %s" % self.enableCaching)
            logging.info("--> Local caching dir         = %s" % self.cacheDir)
            logging.info("--> Max download threads      = %i" % self.downloadThreads)
            logging.info("--> Path to SRA Toolkit Utils = %s" % self.sraPath)
            logging.info("--> fastq-dump options        = %s" % self.fastqDumpOpts)

            assert(self.downloadThreads > 0)

        else:
            ERROR("--> unable to access input file")

    #---
    # download SRA files from a list contained in text file
    def downloadSRA_Files(self):
        logging.warn("\nDownload SRA Files:")
        logging.info("Checking on SRA files specified in -> %s" % self.sraFileName)

        self.sraFiles = []

        if os.path.isfile(self.sraFileName):
            with open(self.sraFileName,'r') as filehandle:
                contents = filehandle.readlines()
                filehandle.close()
        else:
            ERROR("Unable to read SRA input file" % self.sraFileName)

        # create cache directory
        if not os.path.isdir(self.cacheDir):
            logging.warn("\nCreating cache directory for local downloads/processing -> %s" % self.cacheDir)
            try:
                os.makedirs(self.cacheDir,exist_ok=True)
            except:
                ERROR("Unable to create directory -> %s" % self.cacheDir)


        self.fileList = [x.strip() for x in contents]

        if self.enableCaching:

            # check if files already exist locally (in cache mode)
            for file in self.fileList:
                localPath=self.cacheDir + '/' + file + '.sra'

                if not self.checkFilePath(localPath):
                    self.sraFiles.append(file)
        else:
            self.sraFiles = self.fileList

        for entry in self.sraFiles:
            logging.debug("    --> %s" % entry)

        # downlad SRA files (in parallel)

        assert(self.downloadThreads > 0)
        assert(self.downloadThreads < self.maxThreadsAllowed )

        pool = multiprocessing.Pool(self.downloadThreads)

        self.atimer.start("Download SRA Files")
        pool.map(self.downloadFile,self.sraFiles)

        self.atimer.end("Download SRA Files")
        

    #---
    # download specific SRA file from NCBI web site
    def downloadFile_orig(self,name):
        # NCBI organizes download path based on hierarchy, e.g.
        # SRR2296813 -> SRR/SRR229/SRR2296813/SRR2296813.sra

        logging.info("    --> initiating download of SRA file = %s" % name)

        assert(len(name) == 10)
        
        url = name[:3] + '/' + name[:6] + '/' + name + '/' + name + '.sra'
        url = self.ncbiUrl + url

        logging.debug('          --> url = %s' %  url)
        localFile=self.cacheDir + '/' + name + '.sra'
        urllib.request.urlretrieve(url,filename=localFile)

        logging.warn("    <-- completed download of SRA file  = %s" % name)

    def downloadFile(self,name):

        logging.info("    --> initiating download of SRA file = %s" % name)

        assert(len(name) == 10)

        cmd = [self.sraPath + "/" + 'prefetch']
        cmd.append(name)
        cmd.append("-O")
        cmd.append(self.cacheDir)
        logging.info("\nprefetch command -> %s\n" % cmd)

        p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        stdout,stderr = p.communicate()
        logging.debug(stdout.decode())
        if stderr.decode() is not '':
            logging.info(stderr.decode())
        

        localPath=self.cacheDir + '/' + name + '.sra'
        if not os.path.isfile(localPath):
            logging.error("expected sra file not downloaded -> %s" % localPath)
            return 1
        logging.warn("    <-- completed download of SRA file  = %s" % name)
        return 0

    #---
    # convert SRA files with fastq-dump 
    def convertSRA_Files(self):
        logging.warn("\nConverting SRA Files to fastq format:")
        logging.info("Checking on SRA files specified in -> %s" % self.sraFileName)

        # cache directory should already exist
        assert os.path.isdir(self.cacheDir)

        fastqFiles = []

        if self.enableCaching:

            # check if files already exist locally (in cache mode)
            for file in self.fileList:

                localPath=self.cacheDir + '/' + file + '_1.fastq'
                if not self.checkFilePath(localPath):
                    fastqFiles.append(file)
                    continue
                
                localPath=self.cacheDir + '/' + file + '_2.fastq'
                if not self.checkFilePath(localPath):
                    fastqFiles.append(file)
        else:
            fastqFiles = self.fileList

        for entry in fastqFiles:
            logging.debug("    --> %s" % entry)

        # downlad SRA files (in parallel)

        assert(self.downloadThreads > 0)
        assert(self.downloadThreads < self.maxThreadsAllowed )

        pool = multiprocessing.Pool(self.downloadThreads)

        self.atimer.start("Convert SRA Files (%s threads)" % self.downloadThreads)
        workerStatus = pool.map(self.convertSRA_File,fastqFiles)
        if 1 in workerStatus:
            ERROR("\nERROR: One or more fastq conversions failed")

        self.atimer.end("Convert SRA Files (%s threads)" % self.downloadThreads)

    #---
    # convert individual SRA file
    def convertSRA_File(self,name):

        fastqBinary = 'fastq-dump'

        logging.info("    --> initiating fastq conversion of SRA file = %s (binary = %s)" % (name,fastqBinary))

        cmd = [self.sraPath + "/" + fastqBinary]
        cmd.append("--outdir")
        cmd.append(self.cacheDir)

        cmd = cmd + self.fastqDumpOpts.split()
        cmd.append(self.cacheDir + '/' + name + '.sra' )


        logging.debug("\nfastq binary conversion command -> %s\n" % cmd)
        p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        stdout,stderr = p.communicate()
        logging.debug(stdout.decode())
        if stderr.decode() is not '':
            logging.info(stderr.decode())


        localPath=self.cacheDir + '/' + name + '_1.fastq'
        if not os.path.isfile(localPath):
            logging.error("expected fastq file not created-> %s" % localPath)
            return 1
            

        localPath=self.cacheDir + '/' + name + '_2.fastq'
        if not os.path.isfile(localPath):
            logging.error("expected fastq file not created-> %s" % localPath)
            return 1

        logging.warn("    <-- completed fastq conversion of SRA file  = %s" % name)
        return 0

        
        
def main():

    tool = coral_analysis_tool()
    tool.parseConfig()
    tool.downloadSRA_Files()
    tool.convertSRA_Files()


if __name__ == '__main__':
    main()

